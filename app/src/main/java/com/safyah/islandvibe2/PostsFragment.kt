

package com.safyah.islandvibe2

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.View
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.activity_main.*
import android.provider.AlarmClock.EXTRA_MESSAGE
import android.widget.EditText
import android.content.Intent




/**
 * Created by kelvin on 14/10/2017.
 */
class PostsFragment : Fragment() {
    var postsAdapter: PostsAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        fetchPosts()
        postsAdapter?.setOnBottomReachedListener({ post ->
            Log.d("PostFragment", "At the bottom " + post.id)
            fetchPosts(post.id)
        })

        postsAdapter?.postSelectedListener = object : PostsAdapter.PostSelectedListener {
            override fun onVideoSelected(post: Post?) {
                Log.d("PostsFragment", "Video selected ${post?.youtubeId}")
                /*
                val intent = Intent(activity, YouTubeActivity::class.java)
                intent.putExtra("youtubeId", post?.youtubeId)
                startActivity(intent)*/

                val fragment = PostDetailFragment()
                var args = Bundle()
                args.putString("youtubeId", post?.youtubeId)
                args.putString("content", post?.content)
                args.putLong("createdAt", post?.createdAt!!)
                args.putString("displayName", post.displayName)
                args.putString("userPhoto", post?.userPhoto)
                args.putString("country", post?.country)
                fragment.arguments = args
                val transaction = activity.supportFragmentManager.beginTransaction()
                transaction
                        .replace(R.id.fragmentContainer, fragment, "PostDetail")
                        .addToBackStack("PostDetail")
                transaction.commit()
            }

            override fun onPostSelected(post: Post?) {
                Log.d("PostsFragment", "Post selected ${post?.id}")
                val fragment = PostDetailFragment()
                var args = Bundle()
                args.putString("youtubeId", post?.youtubeId)
                args.putString("content", post?.content)
                args.putLong("createdAt", post?.createdAt!!)
                args.putString("displayName", post.displayName)
                args.putString("userPhoto", post?.userPhoto)
                args.putString("country", post?.country)
                fragment.arguments = args
                val transaction = activity.supportFragmentManager.beginTransaction()
                transaction
                        .replace(R.id.fragmentContainer, fragment, "PostDetail")
                        .addToBackStack("PostDetail")
                transaction.commit()
            }

        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val layout = inflater?.inflate(R.layout.fragment_posts, container, false)

        val recyclerView = layout?.findViewById<RecyclerView>(R.id.postsList)
        val mLayoutManager = LinearLayoutManager(activity.applicationContext)
        recyclerView?.layoutManager = mLayoutManager
        recyclerView?.itemAnimator = DefaultItemAnimator()
        recyclerView?.adapter = postsAdapter

        activity.toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp)
        //toolbar?.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp)
        activity.toolbar.setNavigationOnClickListener(View.OnClickListener {

            activity.onBackPressed()
        })
        return layout
    }

    fun fetchPosts(startAt: String = "") {
        var posts = mutableListOf<Post>()

        Log.d("PostsFragment", " startAt " + startAt)

        val dbRef = FirebaseDatabase.getInstance().getReference("posts")
        dbRef.keepSynced(true)

        if(startAt.isEmpty()) {
            dbRef.orderByKey().limitToFirst(20)?.addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError?) {}

                override fun onDataChange(dataSnapshot: DataSnapshot?) {
                    dataSnapshot?.children?.forEach({ snap ->
                        val post = snap.getValue(Post::class.java)
                        Log.d("PostsFragment", "Place " + post?.userId)

                        posts.add(post!!)
                    })

                    Log.d("PostsFragment", posts[0].id)


                    postsAdapter?.addPosts(posts)
                    Log.d("PostsFragment", "Posts length " + postsAdapter?.postsList?.size)

                    postsAdapter?.notifyDataSetChanged()

                }
            })
        }
        else {
            dbRef.orderByKey().startAt(startAt)?.limitToFirst(20)?.addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError?) {}

                override fun onDataChange(dataSnapshot: DataSnapshot?) {
                    dataSnapshot?.children?.forEach({ snap ->
                        val post = snap.getValue(Post::class.java)

                        posts.add(post!!)
                    })


                    Log.d("PostsFragment", posts[0].id)

                    postsAdapter?.addPosts(posts.subList(1,posts.size - 1))
                    Log.d("PostsFragment", "Posts length " + postsAdapter?.postsList?.size)

                    postsAdapter?.notifyDataSetChanged()

                }
            })
        }

        if (postsAdapter == null)
            postsAdapter = PostsAdapter(this, posts)
    }
}