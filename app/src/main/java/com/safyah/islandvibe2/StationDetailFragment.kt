package com.safyah.islandvibe2

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.ScrollView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.joanzapata.iconify.widget.IconTextView
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.activity_main.*


/**
 * Created by kelvin on 14/10/2017.
 */
class StationDetailFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val layout = inflater?.inflate(R.layout.fragment_station_detail, container, false)
        val toolbar = layout?.findViewById<Toolbar>(R.id.toolbar)
        val tabs = layout?.findViewById<TabLayout>(R.id.scheduleTabs)
        val pager = layout?.findViewById<ViewPager>(R.id.schedulePager)

        Log.d("StationDetailFragment", "tag: " + arguments.getString("tag"))
        pager?.adapter = SchedulePagerAdapter(
                childFragmentManager,
                listOf("Mon", "Tue","Wed","Thu", "Fri", "Sat", "Sun"),
                arguments.getString("tag"))
        tabs?.setupWithViewPager(pager)

        /*
        for(i in 1..20) {
            val textView = TextView(activity)
            textView.setText("Hello ${i}")

            textView.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            list?.addView(textView);
        }*/


        val title = layout?.findViewById<TextView>(R.id.title)
        val picture = layout?.findViewById<CircleImageView>(R.id.logo)
        val listenerCount = layout?.findViewById<IconTextView>(R.id.listenerCount)

        listenerCount?.setText("{fa-headphones} " + arguments.getInt("listenerCount"))
        title?.setText(arguments.getString("title"))
        Glide.with(this).load(arguments.getString("picture")).into(picture)


        activity.toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp)
        //toolbar?.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp)
        activity.toolbar.setNavigationOnClickListener(View.OnClickListener {

            activity.onBackPressed()
        })
        return layout
    }
}