package com.safyah.islandvibe2

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.widget.Toast
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import android.support.customtabs.CustomTabsClient
import android.content.ComponentName
import android.support.customtabs.CustomTabsCallback
import android.support.customtabs.CustomTabsServiceConnection
import android.view.*
import kotlinx.android.synthetic.main.activity_main.*


/**
 * Created by kelvin on 14/10/2017.
 */
class NewsFragment : Fragment(), View.OnClickListener {

    var newsAdapter: NewsAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fetchStories()
        newsAdapter?.setOnBottomReachedListener({ story ->
            Log.d("NewsFragment", "At the bottom " + story.name)
            fetchStories(story.count)
        })

    }

    fun fetchStories(endAt: Int = 0) {
        var stories = mutableListOf<Story>()

        Log.d("NewsFragment", " endAt " + endAt)

        val dbRef = FirebaseDatabase.getInstance().getReference("news")
        dbRef.keepSynced(true)
        if(endAt == 0) {
            dbRef.orderByChild("count")?.limitToLast(20)?.addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError?) {}

                override fun onDataChange(dataSnapshot: DataSnapshot?) {
                    dataSnapshot?.children?.forEach({ snap ->
                        val story = snap.getValue(Story::class.java)
                        stories.add(story!!)
                    })

                    Log.d("NewsFragment", stories[0].name)

                    stories.sortByDescending { it -> it.count }
                    newsAdapter?.addStories(stories)
                    Log.d("NewsFragment", "Stories length " + newsAdapter?.newsList?.size)

                    newsAdapter?.notifyDataSetChanged()

                }
            })
        }
        else {
            dbRef.orderByChild("count")?.endAt(endAt.toDouble())?.limitToLast(20)?.addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError?) {}

                override fun onDataChange(dataSnapshot: DataSnapshot?) {
                    dataSnapshot?.children?.forEach({ snap ->
                        val story = snap.getValue(Story::class.java)
                        stories.add(story!!)
                    })

                    Log.d("NewsFragment", stories[0].name)
                    stories.sortByDescending { it -> it.count }

                    newsAdapter?.addStories(stories.subList(1,stories.size - 1))
                    Log.d("NewsFragment", "Stories length " + newsAdapter?.newsList?.size)

                    newsAdapter?.notifyDataSetChanged()

                }
            })
        }

        if (newsAdapter == null)
            newsAdapter = NewsAdapter(this, stories)
    }

    override fun onClick(v: View?) {


        when(v?.id) {
            R.id.news -> {
                //Toast.makeText(this.activity, "News Clicked", Toast.LENGTH_SHORT).show()
                val intent = Intent(this.activity, NewsActivity::class.java)
                startActivity(intent)


            }

            R.id.events -> {
                //Toast.makeText(this.activity, "Events Clicked", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        activity.toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp)
        //toolbar?.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp)
        activity.toolbar.setNavigationOnClickListener(View.OnClickListener {

            activity.onBackPressed()
        })

        // Inflate the layout for this fragment
        val layout = inflater?.inflate(R.layout.fragment_news, container, false)
        val recyclerView = layout?.findViewById<RecyclerView>(R.id.newsList)
        val mLayoutManager = LinearLayoutManager(activity.applicationContext)
        recyclerView?.layoutManager = mLayoutManager
        recyclerView?.itemAnimator = DefaultItemAnimator()
        recyclerView?.adapter = newsAdapter



        return layout;
    }
}