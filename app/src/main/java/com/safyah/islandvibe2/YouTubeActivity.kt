package com.safyah.islandvibe2

import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import com.google.android.youtube.player.YouTubeBaseActivity
import com.google.android.youtube.player.YouTubeInitializationResult
import com.google.android.youtube.player.YouTubePlayer
import com.google.android.youtube.player.YouTubePlayerView

/**
 * Created by kelvin on 10/22/17.
 */

class YouTubeActivity : YouTubeBaseActivity(), YouTubePlayer.OnInitializedListener  {
    override fun onInitializationSuccess(p0: YouTubePlayer.Provider?, player: YouTubePlayer?, wasRestored: Boolean) {
        if (!wasRestored) {
            player?.cueVideo(youtubeId); // Plays https://www.youtube.com/watch?v=fhWaJi1Hsfo
            Log.d("YoutubeActivity", "Cueing ${youtubeId}")
        }
    }

    override fun onInitializationFailure(p0: YouTubePlayer.Provider?, p1: YouTubeInitializationResult?) {
        Log.d("YoutubeActivity", "Initialization Failure")
    }

    var youTubeView: YouTubePlayerView? = null
    var youtubeId : String? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_youtube);
        Log.d("YoutubeActivity", "onCreate ${intent.extras.getString("youtubeId")}")
        youtubeId = intent.extras.getString("youtubeId")

        youTubeView = findViewById<YouTubePlayerView>(R.id.youtubeView)
        youTubeView?.initialize(YouTubeConfig.YOUTUBE_API_KEY, this)

    }
}
