package com.safyah.islandvibe2

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*


/**
 * Created by kelvin on 14/10/2017.
 */
class DashboardFragment : Fragment(), View.OnClickListener {
    override fun onStart() {
        super.onStart()
        activity.toolbar.setNavigationIcon(null)
    }

    override fun onClick(v: View?) {


        when(v?.id) {
            R.id.news -> {
                //Toast.makeText(this.activity, "News Clicked", Toast.LENGTH_SHORT).show()
                //val intent = Intent(this.activity, NewsActivity::class.java)
                //startActivity(intent)
                activity.supportFragmentManager.beginTransaction()
                        .replace(R.id.fragmentContainer, NewsFragment(), "news")
                        .addToBackStack("news")
                        .commit()


            }

            R.id.events -> {
                //Toast.makeText(this.activity, "Events Clicked", Toast.LENGTH_SHORT).show()
                activity.supportFragmentManager.beginTransaction()
                        .replace(R.id.fragmentContainer, EventsFragment(), "events")
                        .addToBackStack("events")
                        .commit()
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val layout = inflater?.inflate(R.layout.fragment_dashboard, container, false)
        var news = layout?.findViewById<LinearLayout>(R.id.news)
        var events = layout?.findViewById<LinearLayout>(R.id.events)

        news?.setOnClickListener(this)
        events?.setOnClickListener(this)
        return layout;


    }
}