package com.safyah.islandvibe2;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;

import android.net.Uri;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.devbrackets.android.exomedia.AudioPlayer;
import com.devbrackets.android.exomedia.listener.OnBufferUpdateListener;
import com.devbrackets.android.exomedia.listener.OnErrorListener;
import com.devbrackets.android.exomedia.listener.OnPreparedListener;
import com.safyah.islandvibe2.Station;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by kelvin on 25/04/2016.
 */
public class RadioService extends Service implements OnPreparedListener, OnErrorListener, AudioManager.OnAudioFocusChangeListener, OnBufferUpdateListener {

    private final IBinder mBinder = new RadioBinder();
    private Bundle icons = new Bundle();

    private static final String TAG = "VincyRadioService";
    private static final String ACTION_PLAY = "com.safyah.action.PLAY";
    private static int NOTIFICATION_ID = 11;
    AudioPlayer mediaPlayer;
    private Station currentStation;
    private ProgressBar currentProgressbar;
    private List<RadioStateListener> listeners;
    private NotificationManager manager;
    private IntentFilter intentFilter = new IntentFilter(AudioManager.ACTION_AUDIO_BECOMING_NOISY);
    private BecomingNoisyReceiver myNoisyAudioStreamReceiver = new BecomingNoisyReceiver();
    private AudioManager audioManager = null;

    @Override
    public void onAudioFocusChange(int focusChange) {
        Log.d("RadioService", "AudioFocus change " + focusChange);

        switch (focusChange) {
            case AudioManager.AUDIOFOCUS_GAIN:
                // resume playback
                Log.d("RadioService", "AudioFocus gained " + focusChange);
                if (mediaPlayer == null) {
                    initMediaPlayer();
                }
                else if (!mediaPlayer.isPlaying()) {
                    mediaPlayer.start();
                }

                mediaPlayer.setVolume(1.0f, 1.0f);
                break;

            case AudioManager.AUDIOFOCUS_LOSS:
                // Lost focus for an unbounded amount of time: stop playback and release media player
                if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                    mediaPlayer.stopPlayback();

                    mediaPlayer.release();
                    if(listeners != null ) {
                        for (RadioStateListener listener : listeners) {
                            listener.onPlayStopped();
                        }
                    }
                }
                mediaPlayer = null;
                break;

            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                // Lost focus for a short time, but we have to stop
                // playback. We don't release the media player because playback
                // is likely to resume
                if (mediaPlayer.isPlaying()) mediaPlayer.pause();
                break;

            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                // Lost focus for a short time, but it's ok to keep playing
                // at an attenuated level
                if (mediaPlayer.isPlaying()) mediaPlayer.setVolume(0.1f, 0.1f);
                break;
        }
    }

    //@Override
    public void onCompletion(AudioPlayer mediaPlayer) {
        Log.d(TAG, "Media player stopped");
    }

    //@Override
    public boolean onInfo(AudioPlayer mediaPlayer, int i, int i1) {
        Log.d(TAG, "Media Info: " + i + " , " + i1);
        return false;
    }

    //@Override
    public void onBufferingUpdate( int i) {

        Log.d(TAG, "Buffering update " + i);
    }

    public class RadioBinder extends Binder {
        RadioService getService() {
            // Return this instance of LocalService so clients can call public methods
            return RadioService.this;
        }
    }

    public static class BecomingNoisyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (AudioManager.ACTION_AUDIO_BECOMING_NOISY.equals(intent.getAction())) {
                // Pause the playback
                Log.d(TAG, "Becoming noisy");
                if(context != null && ((RadioService) context).mediaPlayer != null) {
                    ((RadioService) context).mediaPlayer.pause();
                    RadioService service = ((RadioService) context);
                    for (RadioStateListener listener : service.listeners) {
                        listener.onPlayStopped();
                    }
                }

            }
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.d("Radio Service", "onBind");
        return mBinder;
    }

    public RadioService() {
        super();
        listeners = new ArrayList<RadioStateListener>();

        Log.d("Radio Service", "Service Constructor");
    }

    //@Override
    public boolean onError(Exception ex) {

        Log.d(TAG, "An error occurred " + ex.toString());
        if(mediaPlayer != null) {
            mediaPlayer.reset();
        }
        return false;
    }

    //@Override
    public void onPrepared() {

        if(mediaPlayer == null) {
            Toast.makeText(this, "Problem playing stream", Toast.LENGTH_SHORT).show();
            return;
        }

        mediaPlayer.start();

        int largeIcon = icons.getInt(currentStation.getTitle());

        final NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setContentTitle("Island Vibe")
                        .setTicker("Playing " + currentStation.getTitle())
                        .setContentText("Playing " + currentStation.getTitle());

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            Glide.with(this).load(currentStation.getLogoURL()).asBitmap().into(new SimpleTarget<Bitmap>(50,50) {
                @Override
                public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                    mBuilder.setLargeIcon(resource);
                    mBuilder.setSmallIcon(R.drawable.ic_stat_radio_waves);
                    mBuilder.setColor(getResources().getColor(R.color.colorPrimary));

// Creates an explicit intent for an Activity in your app
                    Intent resultIntent = new Intent(RadioService.this, MainActivity.class);
                    resultIntent.setAction(Intent.ACTION_MAIN);
                    resultIntent.addCategory(Intent.CATEGORY_LAUNCHER);

                    PendingIntent resultPendingIntent = PendingIntent.getActivity(RadioService.this,0,resultIntent,PendingIntent.FLAG_UPDATE_CURRENT);

                    mBuilder.setContentIntent(resultPendingIntent);
                    NotificationManager mNotificationManager =
                            (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
// mId allows you to update the notification later on.
                    mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());


                    Log.d("Radio Service", "onPrepared");

                    for(RadioStateListener listener : listeners) {
                        listener.onPlayStarted(currentStation);
                    }

                    //registerReceiver(myNoisyAudioStreamReceiver, intentFilter);
                }
            });
            registerReceiver(myNoisyAudioStreamReceiver, intentFilter);

        } else {
// Creates an explicit intent for an Activity in your app

            mBuilder.setSmallIcon(R.drawable.ic_stat_radio_waves);
            mBuilder.setContentText("Playing " + currentStation.getTitle());

            Intent resultIntent = new Intent(RadioService.this, MainActivity.class);
            resultIntent.setAction(Intent.ACTION_MAIN);
            resultIntent.addCategory(Intent.CATEGORY_LAUNCHER);

            PendingIntent resultPendingIntent = PendingIntent.getActivity(RadioService.this,0,resultIntent,PendingIntent.FLAG_UPDATE_CURRENT);

            mBuilder.setContentIntent(resultPendingIntent);
            NotificationManager mNotificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
// mId allows you to update the notification later on.
            mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());


            Log.d("Radio Service", "onPrepared");

            for(RadioStateListener listener : listeners) {
                listener.onPlayStarted(currentStation);
            }
            registerReceiver(myNoisyAudioStreamReceiver, intentFilter);

        }


    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        int value = super.onStartCommand(intent, flags, startId);
        return value;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(mediaPlayer != null) {
            try {
                unregisterReceiver(myNoisyAudioStreamReceiver);
            }
            catch (IllegalArgumentException ex) {}
            NotificationManager mNotificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            mNotificationManager.cancel(NOTIFICATION_ID);
            audioManager.abandonAudioFocus(this);
            mediaPlayer.release();
            mediaPlayer = null;
        }
        Log.d(TAG, "onDestroy");
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        Log.d(TAG, "Low Memory");
    }

    public boolean isPlaying() {
        return this.mediaPlayer != null && this.mediaPlayer.isPlaying();
    }

    public Station getCurrentStation() {
        return this.currentStation;
    }

    private void initMediaPlayer() {

        //currentProgressbar = progressBar;
        if(mediaPlayer == null) {
            mediaPlayer = new AudioPlayer(this);
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        }
        else {
            mediaPlayer.reset();
        }

        try {
            Log.d(TAG, currentStation.getUrl());

            mediaPlayer.setDataSource(Uri.parse(currentStation.getUrl()));
            mediaPlayer.setOnPreparedListener(this);
            mediaPlayer.prepareAsync();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void playStream(Station station, ProgressBar progressBar) {
        Log.d("Radio Service", "Play Stream");

        if(mediaPlayer != null && mediaPlayer.isPlaying()) {
            mediaPlayer.stopPlayback();
            stopStream();
        }

        if(audioManager == null) {
            audioManager = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);
        }

        int result = audioManager.requestAudioFocus(this,
                // Use the music stream.
                AudioManager.STREAM_MUSIC,
                // Request permanent focus.
                AudioManager.AUDIOFOCUS_GAIN);

        if (result != AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
            // Start playback
            Log.d(TAG,"Audio focus request not granted");
            return;
        }


        currentStation = station;
        //currentProgressbar = progressBar;
        if(mediaPlayer == null) {
            mediaPlayer = new AudioPlayer(this);
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        }
        else {
            mediaPlayer.reset();
        }

        try {
            Log.d(TAG, station.getUrl());

            mediaPlayer.setDataSource(Uri.parse(station.getUrl()));
            mediaPlayer.setOnPreparedListener(this);
            mediaPlayer.setOnErrorListener(this);
            //mediaPlayer.setOnCompletionListener(this);
            //mediaPlayer.setOnInfoListener(this);

            //mediaPlayer.setOnBufferUpdateListener(this);
            mediaPlayer.prepareAsync();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void stopStream() {
        this.mediaPlayer.reset();
        for(RadioStateListener listener : listeners) {
            listener.onPlayStopped();
        }

        //stopForeground(true);
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.cancel(NOTIFICATION_ID);

        //unregisterReceiver(myNoisyAudioStreamReceiver);


        try {
            if(myNoisyAudioStreamReceiver != null) {
                unregisterReceiver(myNoisyAudioStreamReceiver);
            }
        }
        catch( Exception e) {}


        audioManager.abandonAudioFocus(this);



    }

    public void addStateListener(RadioStateListener listener) {
        listeners.add(listener);
    }

    public interface RadioStateListener {
        public void onPlayStarted(Station currentStation);
        public void onPlayStopped();
    }
}
