package com.safyah.islandvibe2

import com.google.firebase.database.IgnoreExtraProperties;
import java.util.*

@IgnoreExtraProperties
data class Story(var link: String = "", var name: String = "", var created_time: String = "", var brand: String = "", var picture: String = "", var count: Int = 0, var website: String = "") {}