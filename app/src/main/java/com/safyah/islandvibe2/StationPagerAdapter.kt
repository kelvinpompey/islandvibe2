package com.safyah.islandvibe2

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.util.Log

import com.safyah.islandvibe2.ProfileFragment

class StationPagerAdapter(fm: FragmentManager, var countries: List<String>) : FragmentStatePagerAdapter(fm) {

    override fun getItem(i: Int): Fragment {
        val fragment = StationListFragment()
        val args = Bundle()
        // Our object is just an integer :-P
        args.putString("code", countries[i])
        fragment.arguments = args
        return fragment
    }

    override fun getCount(): Int {
        return countries.size
    }

    override fun getPageTitle(position: Int): CharSequence {
        return countries.get(position)
    }
}