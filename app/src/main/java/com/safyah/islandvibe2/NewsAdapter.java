package com.safyah.islandvibe2;

import android.graphics.Color;
import android.media.Image;
import android.net.Uri;
import android.support.customtabs.CustomTabsIntent;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.joanzapata.iconify.fonts.FontAwesomeIcons;

import org.w3c.dom.Text;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import saschpe.android.customtabs.CustomTabsHelper;
import saschpe.android.customtabs.WebViewFallback;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.ViewHolder> {
    private Fragment context;
    private List<Story> newsList;

    public void addStories(List<Story> stories) {
        if(newsList != null) {
            newsList.addAll(stories);
        }
        else {
            Log.d("NewsAdapter", "newsList is null");
            newsList = new ArrayList<Story>(stories);
        }
    }

    public List<Story> getNewsList() {
        return this.newsList;
    }

    public interface OnBottomReachedListener {

        void onBottomReached(Story position);

    }
    OnBottomReachedListener onBottomReachedListener;

    public void setOnBottomReachedListener(OnBottomReachedListener onBottomReachedListener){

        this.onBottomReachedListener = onBottomReachedListener;
    }


    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView title;
        public ImageView picture;
        public TextView shares;
        public TextView brand;
        public TextView date;

        public ViewHolder(View v) {
            super(v);
            title = v.findViewById(R.id.title);
            picture = v.findViewById(R.id.picture);
            brand = v.findViewById(R.id.brand);
            shares = v.findViewById(R.id.shares);
            date = v.findViewById(R.id.date);

        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public NewsAdapter(Fragment context, List<Story> newsList) {
        this.context = context;
        this.newsList = new ArrayList<Story>();
        this.newsList.addAll(newsList);
    }

    // Create new views (invoked by the layout manager)
    @Override
    public NewsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        View v = (View) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.news_item, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        final Story story = newsList.get(position);
        holder.title.setText(story.getName());
        holder.brand.setText("Source: " + story.getBrand());

        holder.shares.setText("Shares: " + Integer.toString(story.getCount()));

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'+0000'");
        SimpleDateFormat dfOut = new SimpleDateFormat("MMM dd, yyyy");

        Date date;

        try {
            date = df.parse(story.getCreated_time());
            holder.date.setText(dfOut.format(date));

        }
        catch( ParseException ex) {

        }

        Glide.with(context).load(story.getPicture()).into(holder.picture);

        if (position == newsList.size() - 4){

            onBottomReachedListener.onBottomReached(newsList.get(newsList.size() - 1));

        }

        holder.title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomTabsIntent customTabsIntent = new CustomTabsIntent.Builder()
                        .addDefaultShareMenuItem()
                        .setToolbarColor(context.getResources().getColor(R.color.colorPrimary))
                        .setShowTitle(true)
                        //.setCloseButtonIcon(FontAwesomeIcons.fa_backward)
                        .build();

// This is optional but recommended
                CustomTabsHelper.addKeepAliveExtra(context.getActivity(), customTabsIntent.intent);

// This is where the magic happens...
                CustomTabsHelper.openCustomTab(context.getActivity(), customTabsIntent,
                        Uri.parse(story.getLink()),
                        new WebViewFallback());

            }
        });

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return newsList.size();
    }
}


