package com.safyah.islandvibe2

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.util.Log

import com.safyah.islandvibe2.ProfileFragment

class SchedulePagerAdapter(fm: FragmentManager, var days: List<String>, var tag: String) : FragmentStatePagerAdapter(fm) {

    override fun getItem(i: Int): Fragment {
        val fragment = ScheduleFragment()
        val args = Bundle()
        // Our object is just an integer :-P
        args.putString("day", days[i])
        args.putString("tag", tag)
        fragment.arguments = args
        return fragment
    }

    override fun getCount(): Int {
        return 7
    }

    override fun getPageTitle(position: Int): CharSequence {
        return days.get(position)
    }
}