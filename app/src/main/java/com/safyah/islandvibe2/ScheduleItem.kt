package com.safyah.islandvibe2

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties

data class ScheduleItem(var startTime: Long = 0, var title:String = "", var description: String = "", var host: String = "")