package com.safyah.islandvibe2

import android.content.Context
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.view.ViewPager
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.View
import android.util.Log
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_home.*


/**
 * Created by kelvin on 14/10/2017.
 */
class HomeFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val layout = inflater?.inflate(R.layout.fragment_home, container, false)

        val pager2 = layout?.findViewById<ViewPager>(R.id.pager)

        pager2?.adapter = StationPagerAdapter(childFragmentManager, listOf("vc", "lc","jm","bb", "tt", "gd", "dm", "sk", "ag"))

        val tabs2 = layout?.findViewById<TabLayout>(R.id.tabs)
        tabs2?.setupWithViewPager(pager2)


        /*
        val recyclerView = layout?.findViewById<RecyclerView>(R.id.stationList)

        (activity as MainActivity).adapter = StationAdapter(this, (activity as MainActivity).stations)
        (activity as MainActivity).adapter?.radioService  = (activity as MainActivity).service;
        val mLayoutManager = LinearLayoutManager(activity.applicationContext)
        recyclerView?.layoutManager = mLayoutManager
        recyclerView?.itemAnimator = DefaultItemAnimator()
        recyclerView?.adapter = (activity as MainActivity).adapter*/
        Log.d("HomeFragment", "onCreateView")

        return layout;


    }

    fun playStation(station: Station) {
        (activity as MainActivity).playStation(station);
    }

    override fun onStart() {
        super.onStart()
        activity.toolbar.setNavigationIcon(null)

        Log.d("HomeFragment", "HomeFragment onStart")
    }
    override fun onResume() {
        super.onResume()
        Log.d("HomeFragment", "HomeFragment resumed")
    }

    override fun onPause() {
        super.onPause()
        Log.d("HomeFragment", "HomeFragment paused")
    }

    override fun onDestroy() {
        super.onDestroy()

    }
}