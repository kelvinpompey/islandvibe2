package com.safyah.islandvibe2

import com.google.firebase.database.IgnoreExtraProperties;
import java.util.*

@IgnoreExtraProperties
data class Image(var url: String = "")

@IgnoreExtraProperties
data class Coordinates(var lat: Float = 0f, var lng: Float = 0f)

@IgnoreExtraProperties
data class Post(
        var id: String = "",
        var content: String = "",
        var displayName: String = "",
        var createdAt: Long = 0,
        var ogDescription: String = "",
        var ogImage: String = "",
        var ogTitle: String = "",
        var ogUrl: String = "",
        var userId: String = "",
        var likeCount: Long = 0,
        var images: List<Image> = listOf<Image>(),
        var userPhoto: String = "",
        var country: String = "",
        var address: String = "",
        var youtubeId: String = "",
        var hastags: Map<String?, Boolean> = mapOf<String?, Boolean>(),
        var location:Coordinates = Coordinates()) {}