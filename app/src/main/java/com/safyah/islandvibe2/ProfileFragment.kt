package com.safyah.islandvibe2

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.Toolbar
import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*


/**
 * Created by kelvin on 14/10/2017.
 */
class ProfileFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val layout = inflater?.inflate(R.layout.fragment_profile, container, false)
        val toolbar = layout?.findViewById<Toolbar>(R.id.toolbar)

        activity.toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp)
        //toolbar?.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp)
        activity.toolbar.setNavigationOnClickListener(View.OnClickListener {

            activity.onBackPressed()
        })
        return layout
    }
}