package com.safyah.islandvibe2

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.util.Log

import com.safyah.islandvibe2.ProfileFragment

class ImagePagerAdapter(fm: FragmentManager, var images: List<Image>) : FragmentStatePagerAdapter(fm) {

    override fun getItem(i: Int): Fragment {

        val fragment = ImageFragment()
        val args = Bundle()
        // Our object is just an integer :-P
        args.putString("url", images.get(i).url)
        fragment.arguments = args
        return fragment
    }

    override fun getCount(): Int {
        return images.size
    }

    override fun getPageTitle(position: Int): CharSequence {
        return position.toString()
    }
}
