package com.safyah.islandvibe2

import com.google.firebase.database.IgnoreExtraProperties;
import java.util.*

@IgnoreExtraProperties
data class Location(var city: String = "", var country: String = "")

@IgnoreExtraProperties
data class Place(var name: String = "", var location:Location = Location())

@IgnoreExtraProperties
data class Event(var id: String = "", var fbid: String = "", var name: String = "", var start_time: String = "", var picture: String = "", var description: String = "", var end_time: String = "", var place:Place = Place()) {}