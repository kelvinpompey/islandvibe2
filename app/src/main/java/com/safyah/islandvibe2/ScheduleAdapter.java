package com.safyah.islandvibe2;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class ScheduleAdapter extends RecyclerView.Adapter<ScheduleAdapter.MyViewHolder> {

    private List<ScheduleItem> scheduleList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView description, title, host, startTime;

        public MyViewHolder(View view) {
            super(view);
            startTime = (TextView) view.findViewById(R.id.startTime);
            title = (TextView) view.findViewById(R.id.title);
            host = (TextView) view.findViewById(R.id.host);
            description = (TextView) view.findViewById(R.id.description);
        }
    }

    public void setScheduleList(List<ScheduleItem> items) {
        this.scheduleList.clear();
        this.scheduleList.addAll(items);
    }

    public ScheduleAdapter(List<ScheduleItem> scheduleList) {

        this.scheduleList = new ArrayList<ScheduleItem>();
        this.scheduleList.addAll(scheduleList);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.schedule_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        ScheduleItem scheduleItem = scheduleList.get(position);
        holder.title.setText(scheduleItem.getTitle());
        holder.description.setText(scheduleItem.getDescription());
        holder.host.setText(scheduleItem.getHost());
        holder.startTime.setText(Long.toString(scheduleItem.getStartTime()));
    }

    @Override
    public int getItemCount() {
        return scheduleList.size();
    }
}