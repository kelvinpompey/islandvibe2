package com.safyah.islandvibe2

import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.Toolbar
import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_main.*


/**
 * Created by kelvin on 14/10/2017.
 */
class ImageFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val layout = inflater?.inflate(R.layout.fragment_image, container, false)

        val image = layout?.findViewById<ImageView>(R.id.image)
        Glide.with(this).load(arguments.getString("url")).into(image)

        return layout
    }
}