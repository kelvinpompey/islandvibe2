package com.safyah.islandvibe2

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.widget.Toast
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import android.support.customtabs.CustomTabsClient
import android.content.ComponentName
import android.net.Uri
import android.support.customtabs.CustomTabsCallback
import android.support.customtabs.CustomTabsIntent
import android.support.customtabs.CustomTabsServiceConnection
import android.support.v7.widget.GridLayoutManager
import android.view.*
import kotlinx.android.synthetic.main.activity_main.*
import saschpe.android.customtabs.CustomTabsHelper
import saschpe.android.customtabs.WebViewFallback
import java.text.SimpleDateFormat
import java.util.*


/**
 * Created by kelvin on 14/10/2017.
 */
class EventsFragment : Fragment(), View.OnClickListener {

    var eventsAdapter: EventsAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val df = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'-0400'")

        val nowAsISO = df.format(Date())

        fetchEvents(nowAsISO)
        eventsAdapter?.setOnBottomReachedListener({ event ->
            Log.d("EventsFragment", "At the bottom " + event.name)
            fetchEvents(event.start_time)
        })

        eventsAdapter?.setEventSelectedListener { event ->
            Toast.makeText(activity, event.name, Toast.LENGTH_SHORT).show()
            val customTabsIntent = CustomTabsIntent.Builder()
                    .addDefaultShareMenuItem()
                    .setToolbarColor(context.resources.getColor(R.color.colorPrimary))
                    .setShowTitle(true)
                    //.setCloseButtonIcon(FontAwesomeIcons.fa_backward)
                    .build()


            CustomTabsHelper.addKeepAliveExtra(activity, customTabsIntent.intent)
            if(event.fbid != null) {
                val url = "https://facebook.com/events/${event.fbid}"
                val intent1 = Intent(Intent.ACTION_VIEW)
                intent1.data = Uri.parse(url)
                startActivity(intent1)
            }
            else {
                CustomTabsHelper.openCustomTab(activity, customTabsIntent,
                        Uri.parse("https://islandvibeapp.com/events?id=${event.id}"),
                        WebViewFallback());
            }



        }

    }

    fun fetchEvents(startAt: String = "") {
        var events = mutableListOf<Event>()

        Log.d("EventsFragment", " startAt " + startAt)

        val dbRef = FirebaseDatabase.getInstance().getReference("events")
        dbRef.keepSynced(true)

        if(startAt.isEmpty()) {
            dbRef.orderByChild("start_time")?.limitToFirst(20)?.addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError?) {}

                override fun onDataChange(dataSnapshot: DataSnapshot?) {
                    dataSnapshot?.children?.forEach({ snap ->
                        val event = snap.getValue(Event::class.java)
                        Log.d("EventsFragment", "Place " + event?.place?.location?.country)

                        events.add(event!!)
                    })

                    Log.d("EventsFragment", events[0].name)


                    eventsAdapter?.addEvents(events)
                    Log.d("EventsFragment", "Events length " + eventsAdapter?.eventsList?.size)

                    eventsAdapter?.notifyDataSetChanged()

                }
            })
        }
        else {
            dbRef.orderByChild("start_time")?.startAt(startAt)?.limitToFirst(20)?.addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError?) {}

                override fun onDataChange(dataSnapshot: DataSnapshot?) {
                    dataSnapshot?.children?.forEach({ snap ->
                        val event = snap.getValue(Event::class.java)
                        Log.d("EventsFragment", "Place " + event?.place?.location?.country)
                        events.add(event!!)
                    })

                    Log.d("EventsFragment", events[0].name)

                    eventsAdapter?.addEvents(events.subList(1,events.size - 1))
                    Log.d("EventsFragment", "Stories length " + eventsAdapter?.eventsList?.size)

                    eventsAdapter?.notifyDataSetChanged()

                }
            })
        }

        if (eventsAdapter == null)
            eventsAdapter = EventsAdapter(this, events)
    }

    override fun onClick(v: View?) {


        when(v?.id) {
            R.id.news -> {
                Toast.makeText(this.activity, "News Clicked", Toast.LENGTH_SHORT).show()
                val intent = Intent(this.activity, NewsActivity::class.java)
                startActivity(intent)


            }

            R.id.events -> {
                Toast.makeText(this.activity, "Events Clicked", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment

        activity.toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp)
        //toolbar?.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp)
        activity.toolbar.setNavigationOnClickListener(View.OnClickListener {

            activity.onBackPressed()
        })

        val layout = inflater?.inflate(R.layout.fragment_events, container, false)
        val recyclerView = layout?.findViewById<RecyclerView>(R.id.eventsList)
        val mLayoutManager = LinearLayoutManager(activity.applicationContext)
        val gridLayoutManager = GridLayoutManager(activity.applicationContext, 2)
        recyclerView?.layoutManager = gridLayoutManager
        recyclerView?.itemAnimator = DefaultItemAnimator()
        recyclerView?.adapter = eventsAdapter

        return layout;
    }
}