package com.safyah.islandvibe2;

import android.content.Context;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.joanzapata.iconify.widget.IconTextView;

import java.util.ArrayList;
import java.util.List;

public class StationAdapter extends RecyclerView.Adapter<StationAdapter.MyViewHolder> {

    private List<Station> stationList;
    private Fragment context;
    private Station selectedStation;
    private RadioService radioService;

    public void setStationSelectedListener(StationSelectedListener stationListener) {
        this.stationSelectedListener = stationListener;
    }

    public void setStations(List<Station> stations) {
        if(stations != null) {
            stationList.clear();
            stationList.addAll(stations);
        }
    }

    public List<Station> getStationList() {
        return this.stationList;
    }

    public interface StationSelectedListener {
        void calendarClicked(Station station);
    }

    StationSelectedListener stationSelectedListener;

    public void setRadioService(RadioService service) {
        this.radioService = service;
    }

    public RadioService getRadioService() {
        return this.radioService;
    }

    /*
    public View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            int position = (int) v.getTag();
            Station station = stationList.get(position);
            Toast.makeText(context.getActivity(), station.getTitle(), Toast.LENGTH_SHORT).show();
            if(StationAdapter.this.radioService != null) {

                StationAdapter.this.radioService.playStream(station,null);
            }
            else {

                //Toast.makeText(context.getActivity(), "Service null", Toast.LENGTH_SHORT).show();
            }
        }
    };
    */

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        public ImageView logo;
        public Station station;
        public IconTextView listenerCount;
        public IconTextView calendar;

        public MyViewHolder(View view) {
            super(view);

            /*
            view.setOnClickListener(new View.OnClickListener(){

                @Override
                public void onClick(View v) {
                    if (clickListener != null) {
                        //clickListener.itemClicked(v, getAdapterPosition());
                    }
                }
            }); */


            title = (TextView) view.findViewById(R.id.titleView);
            logo = (ImageView) view.findViewById(R.id.logo);
            listenerCount = (IconTextView) view.findViewById(R.id.listenerCount);
            //calendar = (IconTextView) view.findViewById(R.id.calendar);

            View.OnClickListener listener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    /*
                    if(v.getId() == R.id.titleView) {

                        FragmentTransaction transaction = context.getActivity().getSupportFragmentManager().beginTransaction();
                        StationDetailFragment fragment = new StationDetailFragment();

                        Station station = stationList.get(getAdapterPosition());
                        Bundle args = new Bundle();
                        args.putString("picture", station.getLogoURL());
                        args.putString("title", station.getTitle());
                        args.putString("tag", station.getTag());
                        args.putInt("listenerCount", station.getListenerCount());
                        args.putString("country", station.getCountryName());

                        fragment.setArguments(args);

                        transaction
                                .replace(R.id.fragmentContainer,fragment,"station-detail")
                                .addToBackStack("station-detail")
                                .commit();

                        return;
                    } */

                    //clickListener.itemClicked(v, getAdapterPosition());
                    int position = getAdapterPosition();

                    if(stationList.size() > position) {
                        Station station = null;
                        try {
                            station = stationList.get(position);
                        }
                        catch(Exception ex) {
                            Toast.makeText(context.getActivity(), "Problem playing stream", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        Toast.makeText(context.getActivity(), station.getTitle(), Toast.LENGTH_SHORT).show();
                        if (StationAdapter.this.radioService != null) {
                            StationAdapter.this.radioService.playStream(station, null);
                        } else {
                            RadioService service = ((MainActivity) context.getActivity()).getService();
                            if (service != null) {
                                service.playStream(station, null);
                            }

                            //Toast.makeText(context.getActivity(), "Service null", Toast.LENGTH_SHORT).show();
                        }
                    }
                    else {
                        Toast.makeText(context.getActivity(), "Error playing station", Toast.LENGTH_SHORT).show();
                    }
                }
            };

            title.setOnClickListener(listener);

            logo.setOnClickListener(listener);

            /*
            calendar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Station station = stationList.get(getAdapterPosition());
                    stationSelectedListener.calendarClicked(station);
                }
            });*/

        }
    }


    public StationAdapter(Fragment context, List<Station> stationList) {
        this.stationList = new ArrayList<Station>();
        this.stationList.addAll(stationList);
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.station_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Station station = stationList.get(position);
        holder.title.setText(station.getTitle());
        holder.title.setTag(position);
        holder.listenerCount.setText("{fa-headphones} " + station.getListenerCount());
        holder.station = stationList.get(position);

        /*
        if(station.getHasSchedule() == false) {
            holder.calendar.setText("");
        }
        else {
            holder.calendar.setText("{fa-calendar}");
        }*/
        Glide.with(context).load(station.getLogoURL()).into(holder.logo);



    }

    @Override
    public int getItemCount() {
        return stationList.size();
    }
}