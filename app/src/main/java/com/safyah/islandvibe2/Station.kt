package com.safyah.islandvibe2

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
data class Station(
        var url: String = "",
        var logoURL: String = "",
        var tag: String = "",
        var title: String = "",
        var listenerCount: Int = 0,
        var countryCode: String = "",
        var countryName: String = "",
        var hasSchedule: Boolean = false) {}

