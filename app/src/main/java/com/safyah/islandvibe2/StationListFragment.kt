package com.safyah.islandvibe2

import android.content.Context
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.View
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.widget.Toast
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.fragment_home.*


/**
 * Created by kelvin on 14/10/2017.
 */
class StationListFragment : Fragment() {

    var dataListener: ValueEventListener? = null
    var database: FirebaseDatabase? = null
    var dbRef : DatabaseReference? = null
    var adapter : StationAdapter? = null
    var stations : MutableList<Station> = mutableListOf<Station>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d("StationListFragment", "onCreate")

        //val code = arguments.get("code")

        fetchStations()
        //val filteredStations = stations.filter { it -> it.countryCode.equals(code) }

        Log.d("StationListFragment",  "onCreate")
        //stations.add(Station("784wave", logoURL = "https://firebasestorage.googleapis.com/v0/b/islandvibe-addd0.appspot.com/o/stations%2F784wave.png?alt=media&token=ce7f404c-0f37-482c-bc30-85bd495c8298"))
        adapter = StationAdapter(this, stations)
        adapter?.radioService  = (activity as MainActivity).service;

        adapter?.setStationSelectedListener { station ->
            Toast.makeText(activity, "Calendar for ${station.title}", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onStart() {
        super.onStart()
        Log.d("StationListFragment", "onStart")

    }

    override fun onResume() {
        super.onResume()
        Log.d("StationListFragment", "onStart")

    }

    override fun onDestroyView() {
        super.onDestroyView()
        Log.d("StationListFragment", "onDestroyView")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("StationListFragment", "onDestroy")
        if(dataListener != null) {
            dbRef?.removeEventListener(dataListener)
            stations?.clear()

        }

    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val layout = inflater?.inflate(R.layout.station_list, container, false)

        val recyclerView = layout?.findViewById<RecyclerView>(R.id.stationList)

        val gridLayoutManager = GridLayoutManager(activity.applicationContext, 3)
        val mLayoutManager = LinearLayoutManager(activity.applicationContext)
        recyclerView?.layoutManager = gridLayoutManager
        recyclerView?.itemAnimator = DefaultItemAnimator()
        recyclerView?.adapter = adapter
        Log.d("HomeFragment", "StationListFragment onCreateView " + stations.size)
        return layout;
    }

    fun playStation(station: Station) {
        (activity as MainActivity).playStation(station);
    }

    override fun onPause() {
        super.onPause()
        Log.d("HomeFragment", "HomeFragment paused")
    }

    fun fetchStations() {
        database = FirebaseDatabase.getInstance()
        dbRef = database?.getReference("stations")
        dbRef?.keepSynced(true)
        val code = arguments.getString("code")

        dataListener = dbRef?.orderByChild("countryCode")?.equalTo( code)?.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                stations.clear()
                dataSnapshot.children.forEach( { snap ->
                    val station = snap.getValue(Station::class.java)
                    stations.add(station!!)

                })

                stations.sortBy { it.listenerCount  }
                stations.reverse()
                adapter?.setStations(stations)
                Log.d("StationListFragment", "got new data " + adapter?.stationList?.size)

                Log.d("StationListFragment", "dataSet changed " + adapter)

                adapter?.notifyDataSetChanged()

            }

            override fun onCancelled(error: DatabaseError) {
                // Failed to read value
                Log.w("MainActivity", "Failed to read value.", error.toException())
            }
        })
    }
}