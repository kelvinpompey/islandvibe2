package com.safyah.islandvibe2

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.support.design.widget.BottomNavigationView
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.devbrackets.android.exomedia.AudioPlayer
import kotlinx.android.synthetic.main.activity_main.*
import android.content.Context.BIND_AUTO_CREATE
import android.content.Intent
import android.content.ComponentName
import android.os.IBinder
import android.content.ServiceConnection
import android.graphics.Color
import android.os.Build
import android.support.annotation.RequiresApi
import android.view.Menu
import com.joanzapata.iconify.IconDrawable
import com.joanzapata.iconify.fonts.FontAwesomeIcons
import android.support.design.widget.Snackbar
import android.support.v4.app.FragmentManager
import android.view.MenuItem
import kotlinx.android.synthetic.main.fragment_home.*
import com.google.android.exoplayer2.upstream.cache.CacheUtil.getKey
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.MobileAds
import com.google.firebase.database.*


class MainActivity : AppCompatActivity(), RadioService.RadioStateListener {

    public val TAG = "MainActivity"
    var player : AudioPlayer? = null
    var currentStation : Station? = null
    var service: RadioService? = null
    var mBound: Boolean = false

    private val connection = object : ServiceConnection {

        override fun onServiceConnected(className: ComponentName,
                                        service: IBinder) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            val binder = service as RadioService.RadioBinder
            this@MainActivity.service = binder.service

            //currentStation = MainActivity.this.service.getCurrentStation();
            mBound = true
            Log.d("MainActivity", "onServiceConnected ")
            //this@MainActivity.adapter?.setRadioService(binder.service)
            this@MainActivity.service?.addStateListener(this@MainActivity)

        }

        override fun onServiceDisconnected(arg0: ComponentName) {
            mBound = false
            Log.d("MainActivity", "onServiceDisconnected")

        }
    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_home -> {


                val fragment = HomeFragment()
                val fragmentManager = supportFragmentManager
                for(i in 0 .. fragmentManager.backStackEntryCount) {
                    fragmentManager.popBackStack();
                }

                val transaction = fragmentManager.beginTransaction()

                transaction.replace(R.id.fragmentContainer, fragment, "home")
                transaction.commit()

                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_dashboard -> {

                val fragment = DashboardFragment()
                val fragmentManager = supportFragmentManager

                for(i in 0 .. fragmentManager.backStackEntryCount) {
                    fragmentManager.popBackStack();
                }

                val transaction = fragmentManager.beginTransaction()

                transaction.replace(R.id.fragmentContainer, fragment)
                transaction.commit()
                return@OnNavigationItemSelectedListener true
            }
            /*
            R.id.navigation_posts -> {
                val fragment = PostsFragment()
                val fragmentManager = supportFragmentManager

                for(i in 0 .. fragmentManager.backStackEntryCount) {
                    fragmentManager.popBackStack();
                }

                val transaction = fragmentManager.beginTransaction()

                transaction.replace(R.id.fragmentContainer, fragment)
                transaction.commit()
                return@OnNavigationItemSelectedListener true
            }
            */
            /*
            R.id.navigation_notifications -> {

                return@OnNavigationItemSelectedListener true
            }*/
        }
        false
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)

        if (this.service != null && this.service?.isPlaying!!) {
            menu.findItem(R.id.action_stop)
                    .setIcon(IconDrawable(this, FontAwesomeIcons.fa_stop).color(Color.WHITE).actionBarSize())
        } else {
            menu.findItem(R.id.action_stop)
                    .setIcon(IconDrawable(this, FontAwesomeIcons.fa_play).color(Color.WHITE).actionBarSize())
        }

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.getItemId()


        if (id == R.id.action_stop) {

            if (service?.currentStation == null) {
                Snackbar.make(this.container, "Select a station to play", Snackbar.LENGTH_LONG).show()
            } else {
                if (this.service != null) {
                    if (this.service?.isPlaying!!) {
                        this.service?.stopStream()
                    } else {
                        this.service?.playStream(currentStation, null)
                    }
                }
            }
            //IconDrawable icon = new IconDrawable(this,FontAwesomeIcons.fa_stop).color(Color.WHITE).actionBarSize();
            //toolbar.getMenu().findItem(R.id.action_stop).setIcon(icon);

            return true
        }
        /*
        else if (id == R.id.action_info) {
            val station = service?.currentStation

            if(station == null) {
                return true
            }

            val transaction = supportFragmentManager.beginTransaction()
            val fragment = StationDetailFragment();
            val args = Bundle();
            args.putString("picture", station?.logoURL);
            args.putString("title", station?.title);
            args.putString("tag", station?.tag);
            args.putInt("listenerCount", station?.listenerCount!!);
            args.putString("country", station?.countryName);

            fragment.setArguments(args);
            transaction
                    .replace(R.id.fragmentContainer, fragment, "StationDetail")
                    .addToBackStack("StationDetail")
                    .commit();
        }

        */
        return super.onOptionsItemSelected(item)
    }

    fun playStation(station: Station) {
        player?.setDataSource(Uri.parse(station.url))
        currentStation = station
        player?.prepareAsync()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        MobileAds.initialize(this, "ca-app-pub-3588440997094128~5397668178");

        val mAdView = findViewById<AdView>(R.id.adView);
        val adRequest = AdRequest
                .Builder()
                .addTestDevice("F4E34531B3EA798CE6BF3476FE439EFD")
                .build();
        mAdView.loadAd(adRequest);

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        navigation.selectedItemId = R.id.navigation_home

        player = AudioPlayer(this)
        (player as AudioPlayer).setOnPreparedListener({
            (player as AudioPlayer).start()


            this.setTitle("${if(player?.isPlaying!!) "Playing " else "Stopped " } ${currentStation?.title}")
        })

        val intent = Intent(this.applicationContext, RadioService::class.java)
        this.bindService(intent, connection, Context.BIND_AUTO_CREATE)


        getSupportFragmentManager().addOnBackStackChangedListener(
                object : FragmentManager.OnBackStackChangedListener {

                    override fun onBackStackChanged() {
                        // Update your UI here.

                        val f = supportFragmentManager.findFragmentById(R.id.fragmentContainer)
                        Log.d("MainActivity", "Back stack changed " +  f);

                        /*
                        if(f.tag == "home") {
                            supportFragmentManager
                                    .beginTransaction()
                                    .replace(R.id.fragmentContainer, HomeFragment(), "home")
                                    .commit()

                        }
                        */ 
                        // do something with f
                            //(f as CustomFragmentClass).doSomething()
                    }
                })

    }

    override fun onStop() {
        super.onStop()
        Log.d("MainActivity", "Stop")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("MainActivity", "Destroy")
        Log.d(TAG, "MainActivity.onDestroy");

        if(connection != null) {
            unbindService(connection);
        }


        /*

        if(mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }

        */

        if(this.currentStation != null && this.service?.isPlaying!!) {
            Log.d("MainActivity", "Destroying activity")
            decrementListenerCount();
        }
    }

    override fun onPlayStarted(currentStation: Station?) {
        this.currentStation = currentStation
        this.title = "${if ( service?.isPlaying!! ) "Playing " else "Stopped" } ${currentStation?.title}"
        this.supportActionBar?.subtitle = currentStation?.countryName

        val icon  = IconDrawable(this,FontAwesomeIcons.fa_stop).color(Color.WHITE).actionBarSize();


        toolbar.getMenu().findItem(R.id.action_stop).setIcon(icon);
        incrementListenerCount()
    }

    override fun onPlayStopped() {
        this.title = "${if ( service?.isPlaying!! ) "Playing " else "Stopped" } ${currentStation?.title}"
        this.supportActionBar?.subtitle = currentStation?.countryName

        val icon  = IconDrawable(this,FontAwesomeIcons.fa_play).color(Color.WHITE).actionBarSize();


        toolbar.getMenu().findItem(R.id.action_stop).setIcon(icon);
        decrementListenerCount()
    }


    fun incrementListenerCount() {
        val database = FirebaseDatabase.getInstance();

        if(currentStation == null) return
        val stationRef = database.getReference("stations").child(currentStation?.tag)
        stationRef.runTransaction(object : Transaction.Handler {
            override fun doTransaction(mutableData: MutableData): Transaction.Result {
                val station = mutableData.getValue(Station::class.java) ?: return Transaction.success(mutableData)

                station!!.listenerCount = station!!.listenerCount + 1
                mutableData.setValue(station)
                return Transaction.success(mutableData)
            }

            override fun onComplete(databaseError: DatabaseError?, b: Boolean, dataSnapshot: DataSnapshot) {
                if (databaseError != null) {
                    Log.d("MainActivity", "database error")
                }
            }
        })


    }

    fun decrementListenerCount() {
        val database = FirebaseDatabase.getInstance();

        if(currentStation == null) return
        val stationRef = database.getReference("stations").child(currentStation?.tag)
        stationRef.runTransaction(object : Transaction.Handler {
            override fun doTransaction(mutableData: MutableData): Transaction.Result {
                val station = mutableData.getValue(Station::class.java) ?: return Transaction.success(mutableData)

                station!!.listenerCount = station!!.listenerCount - 1
                mutableData.setValue(station)
                return Transaction.success(mutableData)
            }

            override fun onComplete(databaseError: DatabaseError?, b: Boolean, dataSnapshot: DataSnapshot) {
                if (databaseError != null) {
                    Log.d("MainActivity", "database error")
                }
            }
        })
    }

}
