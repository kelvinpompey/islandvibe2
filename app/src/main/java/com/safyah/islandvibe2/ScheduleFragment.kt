package com.safyah.islandvibe2

import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_main.*
import org.w3c.dom.Text


/**
 * Created by kelvin on 14/10/2017.
 */
class ScheduleFragment : Fragment() {

    var scheduleList : MutableList<ScheduleItem> = mutableListOf()
    var infoTextView : TextView? = null
    var adapter : ScheduleAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        fetchSchedule()
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment

        val layout = inflater?.inflate(R.layout.fragment_schedule, container, false)
        val scheduleRecyclerView = layout?.findViewById<RecyclerView>(R.id.scheduleList)

        infoTextView = layout?.findViewById<TextView>(R.id.info)

        adapter = ScheduleAdapter(scheduleList)
        scheduleRecyclerView?.adapter = adapter

        val mLayoutManager = LinearLayoutManager(activity.applicationContext)
        scheduleRecyclerView?.layoutManager = mLayoutManager

        activity.toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp)
        //toolbar?.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp)
        activity.toolbar.setNavigationOnClickListener(View.OnClickListener {

            activity.onBackPressed()
        })
        Log.d("ScheduleFragment", "onCreateView")

        return layout
    }

    fun fetchSchedule() {

        val db = FirebaseFirestore.getInstance()
        Log.d("ScheduleFragment", "tag ${arguments.getString("tag")} day ${arguments.getString("day")}")
        db.collection("station-schedule")
                .whereEqualTo("tag", arguments.getString("tag").toLowerCase())
                .whereEqualTo("day", arguments.getString("day").toLowerCase())
                .get()
                .addOnCompleteListener(OnCompleteListener { task ->
                    if(task.isSuccessful) {
                        for(document in task.result) {
                            Log.d("ScheduleFragment", document.getId() + " => " + document.data.get("title"))
                            val data = document.data
                            val scheduleItem = ScheduleItem(
                                    data.get("startTime") as Long,
                                    data.get("title") as String,
                                    data.get("description") as String,
                                    data.get("host") as String)

                            scheduleList.add(scheduleItem)

                        }

                        scheduleList.sortBy { it -> it.startTime }

                        if(scheduleList.size > 0) {
                            infoTextView?.height = 0
                        }
                        adapter?.setScheduleList(scheduleList);
                        Log.d("ScheduleFragment", "numItems ${adapter?.itemCount}")
                        adapter?.notifyDataSetChanged()
                    }
                })
    }

    fun populateData() {

    }

}