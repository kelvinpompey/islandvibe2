package com.safyah.islandvibe2;

import android.graphics.Color;
import android.media.Image;
import android.net.Uri;
import android.support.customtabs.CustomTabsIntent;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.joanzapata.iconify.fonts.FontAwesomeIcons;

import org.w3c.dom.Text;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import saschpe.android.customtabs.CustomTabsHelper;
import saschpe.android.customtabs.WebViewFallback;

public class EventsAdapter extends RecyclerView.Adapter<EventsAdapter.ViewHolder> {
    private Fragment context;
    private List<Event> eventsList;

    public void addEvents(List<Event> events) {
        if(eventsList != null) {
            eventsList.addAll(events);
        }
        else {
            Log.d("NewsAdapter", "newsList is null");
            eventsList = new ArrayList<Event>(events);
        }
    }

    public List<Event> getEventsList() {
        return this.eventsList;
    }

    public interface EventSelectedListener {
        void onEventSelected(Event evt);
    }

    public interface OnBottomReachedListener {

        void onBottomReached(Event event);

    }
    OnBottomReachedListener onBottomReachedListener;
    EventSelectedListener eventSelectedListener;

    public void setOnBottomReachedListener(OnBottomReachedListener onBottomReachedListener){

        this.onBottomReachedListener = onBottomReachedListener;
    }

    public void setEventSelectedListener(EventSelectedListener listener) {
        this.eventSelectedListener = listener;
    }


    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView title;
        public ImageView picture;
        public TextView startTime;
        public TextView country;

        public ViewHolder(View v) {
            super(v);
            title = v.findViewById(R.id.title);
            startTime = v.findViewById(R.id.startTime);
            country = v.findViewById(R.id.country);
            picture = v.findViewById(R.id.picture);

            v.setBackgroundColor(Color.argb(50,0,0,0));

        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public EventsAdapter(Fragment context, List<Event> eventsList) {
        this.context = context;
        this.eventsList = new ArrayList<Event>();
        this.eventsList.addAll(eventsList);
    }

    // Create new views (invoked by the layout manager)
    @Override
    public EventsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                     int viewType) {
        // create a new view
        View v = (View) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.event_item, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        final Event event = eventsList.get(position);
        holder.title.setText(event.getName());

        holder.country.setText(event.getPlace().getLocation().getCountry());

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'-0400'");
        SimpleDateFormat dfOut = new SimpleDateFormat("MMM dd, yyyy");

        String nowAsISO = "";

        try {
            nowAsISO = dfOut.format(df.parse(event.getStart_time()));
        }
        catch (ParseException ex) {

        }

        holder.startTime.setText(nowAsISO);

        Glide.with(context).load(event.getPicture()).into(holder.picture);

        if (position == eventsList.size() - 4){

            onBottomReachedListener.onBottomReached(eventsList.get(eventsList.size() - 1));

        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                eventSelectedListener.onEventSelected(eventsList.get(position));
            }
        });
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return eventsList.size();
    }
}


