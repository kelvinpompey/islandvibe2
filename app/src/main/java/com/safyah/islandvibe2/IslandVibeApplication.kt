package com.safyah.islandvibe2

import android.app.Application
import android.support.multidex.MultiDexApplication
import com.google.firebase.FirebaseApp
import com.google.firebase.database.FirebaseDatabase
import com.joanzapata.iconify.fonts.IoniconsModule
import com.joanzapata.iconify.fonts.MaterialModule
import com.joanzapata.iconify.fonts.FontAwesomeModule
import com.joanzapata.iconify.Iconify
import saschpe.android.customtabs.CustomTabsActivityLifecycleCallbacks



/**
 * Created by kelvin on 15/10/2017.
 */

class IslandVibeApplication: MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()
        FirebaseApp.initializeApp(this);

        FirebaseDatabase.getInstance().setPersistenceEnabled(true);

        registerActivityLifecycleCallbacks(CustomTabsActivityLifecycleCallbacks())

        Iconify
                .with(FontAwesomeModule())
                .with(MaterialModule())
                .with(IoniconsModule())
    }

}