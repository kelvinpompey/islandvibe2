package com.safyah.islandvibe2

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import com.bumptech.glide.Glide
import com.google.android.youtube.player.YouTubeInitializationResult
import com.google.android.youtube.player.YouTubePlayer
import com.google.android.youtube.player.YouTubePlayerSupportFragment
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.activity_main.*
import java.text.SimpleDateFormat
import java.util.*


/**
 * Created by kelvin on 14/10/2017.
 */
class PostDetailFragment : Fragment(), YouTubePlayer.OnInitializedListener {
    override fun onInitializationSuccess(p0: YouTubePlayer.Provider?, player: YouTubePlayer?, wasRestored: Boolean) {
        if(!wasRestored) {
            Log.d("PostDetailFragment", "YouTubeId: " + arguments.getString("youtubeId"))
            player?.cueVideo(arguments.getString("youtubeId"))

        }
    }

    override fun onInitializationFailure(p0: YouTubePlayer.Provider?, p1: YouTubeInitializationResult?) {
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val layout = inflater?.inflate(R.layout.post_detail, container, false)
        val title = layout?.findViewById<TextView>(R.id.title)
        val profilePicture = layout?.findViewById<CircleImageView>(R.id.profilePicture)
        val displayName = layout?.findViewById<TextView>(R.id.displayName)
        val startTime = layout?.findViewById<TextView>(R.id.startTime)
        val country = layout?.findViewById<TextView>(R.id.country)

        displayName?.setText(arguments.getString("displayName"))
        title?.setText(arguments.getString("content"))
        Glide.with(this).load(arguments.getString("userPhoto")).into(profilePicture)

        val dfOut = SimpleDateFormat("MMM dd, yyyy")

        var nowAsISO = ""

        try {
            nowAsISO = dfOut.format(Date(arguments.getLong("createdAt")))
        } catch (ex: Exception) {

        }

        startTime?.setText(nowAsISO)
        country?.setText(arguments.getString("country"))


        val youtubeFragment = childFragmentManager.findFragmentById(R.id.youtubeFragment) as YouTubePlayerSupportFragment

        Log.d("PostDetailFragment", "YouTubeFragment" + youtubeFragment)

        val youtubeId = arguments.getString("youtubeId")
        if(youtubeId != null && youtubeId.length > 1) {

            youtubeFragment.initialize(YouTubeConfig.YOUTUBE_API_KEY, this)
        }
        activity.toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp)
        //toolbar?.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp)
        activity.toolbar.setNavigationOnClickListener(View.OnClickListener {

            activity.onBackPressed()
        })
        return layout
    }
}