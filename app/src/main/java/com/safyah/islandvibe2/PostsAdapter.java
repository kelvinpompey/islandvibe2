package com.safyah.islandvibe2;

import android.graphics.Color;
import android.net.Uri;
import android.support.customtabs.CustomTabsIntent;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;
import com.google.android.youtube.player.YouTubeThumbnailLoader;
import com.google.android.youtube.player.YouTubeThumbnailView;
import com.joanzapata.iconify.fonts.FontAwesomeIcons;

import org.w3c.dom.Text;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import saschpe.android.customtabs.CustomTabsHelper;
import saschpe.android.customtabs.WebViewFallback;

public class PostsAdapter extends RecyclerView.Adapter<PostsAdapter.ViewHolder> {
    private Fragment context;
    private List<Post> postsList;

    public void addPosts(List<Post> posts) {
        if(postsList != null) {
            postsList.addAll(posts);
        }
        else {
            Log.d("PostsAdapter", "newsList is null");
            postsList = new ArrayList<Post>(posts);
        }
    }

    public List<Post> getPostsList() {
        return this.postsList;
    }

    public interface PostSelectedListener {
        void onPostSelected(Post post);
        void onVideoSelected(Post post);
    }

    public interface OnBottomReachedListener {

        void onBottomReached(Post post);

    }
    OnBottomReachedListener onBottomReachedListener;
    PostSelectedListener postSelectedListener;

    public void setOnBottomReachedListener(OnBottomReachedListener onBottomReachedListener){

        this.onBottomReachedListener = onBottomReachedListener;
    }

    public void setPostSelectedListener(PostSelectedListener listener) {
        this.postSelectedListener = listener;
    }


    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView title;
        public CircleImageView userPhoto;
        public ImageView picture;
        public TextView startTime;
        public TextView country;
        public TextView displayName;
        public ViewPager imagePager;
        public YouTubeThumbnailView youtubeThumbnail;

        public ViewHolder(View v) {
            super(v);
            title = v.findViewById(R.id.title);
            startTime = v.findViewById(R.id.startTime);
            country = v.findViewById(R.id.country);
            userPhoto = v.findViewById(R.id.profilePicture);
            picture = v.findViewById(R.id.picture);
            displayName = v.findViewById(R.id.displayName);
            //imagePager = v.findViewById(R.id.imagePager);
            youtubeThumbnail = v.findViewById(R.id.youtubeThumbnail);

        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public PostsAdapter(Fragment context, List<Post> postsList) {
        this.context = context;
        this.postsList = new ArrayList<Post>();
        this.postsList.addAll(postsList);
    }

    // Create new views (invoked by the layout manager)
    @Override
    public PostsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                       int viewType) {
        // create a new view
        View v = (View) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.post_item, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        final Post post = postsList.get(position);
        holder.title.setText(post.getContent());

        holder.country.setText(post.getCountry());
        holder.displayName.setText(post.getDisplayName());

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'-0400'");
        SimpleDateFormat dfOut = new SimpleDateFormat("MMM dd, yyyy");

        String nowAsISO = "";

        try {
            nowAsISO = dfOut.format(new Date(post.getCreatedAt()));
        }
        catch (Exception ex) {

        }

        List<Image> images = post.getImages();
        if(images.size() > 0 ) {
            Log.d("PostADapter", "Found images " + images.size());
            Glide.with(context).load(images.get(0).getUrl()).into(holder.picture);
            //holder.imagePager.setAdapter(new ImagePagerAdapter(context.getChildFragmentManager(), post.getImages()));
        }
        else {
            holder.picture.setImageBitmap(null);
        }

        holder.startTime.setText(nowAsISO);

        Glide.with(context).load(post.getUserPhoto()).into(holder.userPhoto);

        if (position == postsList.size() - 4){

            onBottomReachedListener.onBottomReached(postsList.get(postsList.size() - 1));

        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postSelectedListener.onPostSelected(postsList.get(position));
            }
        });

        holder.youtubeThumbnail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                postSelectedListener.onVideoSelected(postsList.get(position));
            }
        });

        String youtubeId = post.getYoutubeId();
        if(youtubeId.length() > 1) {
            holder.youtubeThumbnail.initialize(YouTubeConfig.YOUTUBE_API_KEY, new YouTubeThumbnailView.OnInitializedListener() {
                @Override
                public void onInitializationSuccess(YouTubeThumbnailView youTubeThumbnailView, YouTubeThumbnailLoader youTubeThumbnailLoader) {
                    String youtubeId = post.getYoutubeId();

                    youTubeThumbnailLoader.setVideo(post.getYoutubeId());
                    holder.youtubeThumbnail.setMaxHeight(400);


                }

                @Override
                public void onInitializationFailure(YouTubeThumbnailView youTubeThumbnailView, YouTubeInitializationResult youTubeInitializationResult) {

                }
            });
        }
        else {
            holder.youtubeThumbnail.setMaxHeight(0);
            holder.youtubeThumbnail.setImageBitmap(null);
        }

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return postsList.size();
    }
}


