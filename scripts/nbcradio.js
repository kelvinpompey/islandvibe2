var admin = require("firebase-admin");

var serviceAccount = require("./serviceAccountKey.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://islandvibe-addd0.firebaseio.com"
});

let db = admin.firestore() 

console.log('firestore db ', db); 

/*
db.collection("station-schedule").get().then(snap => {
    snap.forEach(doc => {
        console.log('doc', doc.data())
    })
}); */ 

let doc = { 
    day: 'mon',
    host: 'Host: Desmond "Desi" Arrindel',
    title: 'The Good Morning Show',
    startTime: 530,
    description: 'The first shift of the day and the busiest, the morning show is about waking up the nation to fresh information with the first newscast of the day at 7:30, positive music and of course interacting with the listeners. One of the features of this show is the “Face to Face” and “Open forum” programs which seek to encourage healthy and open discussions of issues of national interest.',
    tag: 'nbcradio'
} 

let days = ["mon", "tue", "wed", "thu", "fri", "sat", "sun"] 

var batch = db.batch();

docs = days.forEach((day, index) => {
     doc = { 
        day: day,
        host: 'Desmond "Desi" Arrindel',
        title: 'The Good Morning Show',
        startTime: 530,
        description: 'The first shift of the day and the busiest, the morning show is about waking up the nation to fresh information with the first newscast of the day at 7:30, positive music and of course interacting with the listeners. One of the features of this show is the “Face to Face” and “Open forum” programs which seek to encourage healthy and open discussions of issues of national interest.',
        tag: 'nbcradio'
    } 

    doc2 = { 
        day: day,
        host: 'Javelle Frank',
        title: 'The People and Places Show',
        startTime: 915,
        description: 'The mid morning interlude of music to get you busy at work, a blend of a variety of music mixed with a splash of interactivity, news and sports, within this show falls the “Interface” program which reaches out to the community to highlight current events and one of the most listened to newscasts on radio falls within this show at 12:30 followed by the obituaries as the transition is made into the next show.',
        tag: 'nbcradio'
    }
    
    doc3 = { 
        day: day,
        host: 'Javelle Frank',
        title: 'The Rhythm Roundabout / Carnival Rhythms Show (During Carnival Season)',
        startTime: 1300,
        description: 'An afternoon musical mix with energy to keep you going through that busy day features music of various genres carefully selected to keep you on your feet and moving as well as keeping you updated with the latest news and sports and other current events. The curtains come down at 6PM to make way for the final shift of the day.',
        tag: 'nbcradio'
    }
    
    doc4 = { 
        day: day,
        host: '',
        title: 'The Night Shift',
        startTime: 1800,
        description: 'Features the latest and most comprehensive evening newscast at 6:30 followed by exciting and entertaining selections to tantalize the listeners and help them unwind after a hard day at work and gear down for some much needed rest.At the end of this shift we end our transmission for the day from our studios making way for the BBC World Service until 5 :30 AM',
        tag: 'nbcradio'
    }
    
    doc5 = { 
        day: day,
        host: '',
        title: 'Linking with BBC World Service London',
        startTime: 2300,
        description: 'Keeping up to the minute on what’s happening around the world with the BBC world service radio, NBC is a partner station of the world service and we relay its program when we end our transmission from our studios 7 days a week',
        tag: 'nbcradio'
    }    

    var ref = db.collection("station-schedule").doc((index + 24).toString());    
    batch.set(ref, doc5); 
}); 

batch.commit().then(function () {
    console.log("done writing to db")
});


