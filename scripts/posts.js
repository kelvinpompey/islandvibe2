var admin = require("firebase-admin");

var serviceAccount = require("./serviceAccountKey.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://islandvibe-addd0.firebaseio.com"
});

let db = admin.firestore() 

admin.database().ref('posts').once('value').then(snap => {
    var batch = db.batch(); 
    snap.forEach(ref => {
        const post = ref.val() 
        console.log(ref.key, " => ", post); 

        let ref2 = db.collection('posts').doc(ref.key) 
        batch.set(ref2, post); 

    })

    
    batch.commit().then(() => {
        console.log('done saving posts'); 
    })
})