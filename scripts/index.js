var admin = require("firebase-admin");

var serviceAccount = require("./serviceAccountKey.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://islandvibe-addd0.firebaseio.com"
});

let db = admin.firestore() 

console.log('firestore db ', db); 

/*
db.collection("station-schedule").get().then(snap => {
    snap.forEach(doc => {
        console.log('doc', doc.data())
    })
}); */ 

let doc = { 
    day: 'mon',
    host: 'Host: Desmond "Desi" Arrindel',
    title: 'The Good Morning Show',
    startTime: 530,
    description: 'The first shift of the day and the busiest, the morning show is about waking up the nation to fresh information with the first newscast of the day at 7:30, positive music and of course interacting with the listeners. One of the features of this show is the “Face to Face” and “Open forum” programs which seek to encourage healthy and open discussions of issues of national interest.',
    tag: 'nbcradio'
} 

let alldays = ["mon", "tue", "wed", "thu", "fri", "sat", "sun"] 
let sixdays = ["mon", "tue", "wed", "thu", "fri", "sat"] 
let monToThu = ["mon", "tue", "wed", "thu"] 
let monToFri = ["mon", "tue", "wed", "thu", "fri"] 

var batch = db.batch();

docs = monToThu.forEach((day, index) => {
     doc = { 
        day: day,
        host: '2 Kool Kris with Kubiyashi',
        title: 'AM Mayhem Morning Show',
        startTime: 600,
        description: '',
        tag: 'hot97svg'
    } 

    doc2 = { 
        day: day,
        host: 'Colin “the Hitman” Graham',
        title: 'The Hitman Radio Show',
        startTime: 900,
        description: '',
        tag: 'hot97svg'
    }
    
    doc3 = { 
        day: day,
        host: 'The Colombian',
        title: 'Energy Central 2.0',
        startTime: 1300,
        description: '',
        tag: 'hot97svg'
    }
    
    doc4 = { 
        day: day,
        host: '',
        title: 'Tha Big Dogg, Pitbull alongside Big Daddy Cain',
        startTime: 1600,
        description: '',
        tag: 'hot97svg'
    }
    
    doc5 = { 
        day: day,
        host: 'Shaney Hypz 4000 with DJ Bedz 5000',
        title: 'Club97',
        startTime: 1900,
        description: '',
        tag: 'hot97svg'
    }    

    doc6 = { 
        day: day,
        host: 'Bartez and DJ Slick',
        title: 'Double Click Sounds',
        startTime: 2200,
        description: '',
        tag: 'hot97svg'
    }        

    doc7 = {
        day: 'fri', 
        host: '', 
        title: 'Shaney Hypz 4000 with Yung G', 
        startTime: 900, 
        description: '', 
        tag: "hot97svg"
    }

    doc8 = {
        day: 'sat', 
        host: '', 
        title: 'Colin ‘the Hitman” Graham with Yung G', 
        startTime: 600, 
        description: '', 
        tag: "hot97svg"
    }    

    var ref = db.collection("station-schedule").doc((index + 60).toString());    
    batch.set(ref, doc8); 
}); 

batch.commit().then(function () {
    console.log("done writing to db")
});


